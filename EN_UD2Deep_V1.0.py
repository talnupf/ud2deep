#!/usr/bin/env python
# -*- coding: utf-8 -*-
# authors: simon mille, alex shvets

import os, shutil
from shutil import copyfile
import sys
import glob
import subprocess
from subprocess import Popen, PIPE
import timeit
import datetime
import codecs
import re

start = timeit.default_timer()

#============================================================================================================
# GENERAL PARAMETERS
#============================================================================================================
# path to input folder
inputFolder = 'inputs'
# path to output folder
outputFolder = 'out'
# path to the folder in which the debug info is stored
debugFolder = os.path.join(outputFolder, 'debug')
# file extension: input file must be in 'conllu' format, with or without metadata (lines starting with '#')
inputFormat = 'conllu'
# number of structures per file: big files need to be cut into smaller files containing this amount of sentences (10,000 max recommended); separated files are brought back together at the end of the conversion.
strPerFile = '10000'
# perform structure well-formedness and file alignment checks and create debug files
debug = 'no'

#============================================================================================================
# CONVERSION PARAMETERS ('yes'/'no')
#============================================================================================================
# generate outpus of surface ('t1') or deep ('t2') tracks
track = 't1'
# data type: '1' is for train and dev, '2' is for test
dt = '1'
# scramble files or not, i.e. change order of words ('yes', 'no')
scramble = 'yes'
# keep ID of position of each word from the original conllu file (column #1) in the deep structure (SRST: 'yes' if scrambled)
originalID = 'yes'
# keep relative ordering of punctuation marks (SRST surface: 'yes'; deep: 'no')
orderPunc = 'yes'
if track == 't2':
	orderPunc = 'no'
# keep relative ordering of cunjuncts in coordination (SRST: 'yes')
orderConj = 'yes'
# keep relative ordering of MWE components (SRST: 'yes')
orderMWE = 'yes'


#============================================================================================================
# ADDITIONAL PARAMETERS FOR DEEP STRUCTURES
#============================================================================================================
default = 'no'
# keep form from the original conllu file (column #2) in the deep structure? (SRST: 'no')
originalForm = default
# keep xpos from the original conllu file (column #5) in the deep structure (SRST: 'no')
originalXpos = default
# keep parentheses in the deep structure (SRST: 'no')
parentheses = default
# keep quotation marks in the deep structure (SRST: 'no')
quotationMarks = default


# Clear the debug folder before starting the conversion
try:
	shutil.rmtree(debugFolder)
except Exception as e:
	pass
	
try:
	shutil.rmtree('tmpIn')
except Exception as e:
	pass
	
try:
	shutil.rmtree('tmpOut')
except Exception as e:
	pass

if not os.path.exists(outputFolder):
	os.makedirs(outputFolder)

deepOut = os.path.join(outputFolder, 'deep')
if track == 't2':
	if not os.path.exists(deepOut):
		os.makedirs(deepOut)
surfOut = os.path.join(outputFolder, 'surf')
if not os.path.exists(surfOut):
	os.makedirs(surfOut)
sentOut = os.path.join(outputFolder, 'sent')
if not os.path.exists(sentOut):
	os.makedirs(sentOut)

deepOutTmp = os.path.join('tmpOut', 'deep')
if track == 't2':
	if not os.path.exists(deepOutTmp):
		os.makedirs(deepOutTmp)
surfOutTmp = os.path.join('tmpOut', 'surf')
if not os.path.exists(surfOutTmp):
	os.makedirs(surfOutTmp)
sentOutTmp = os.path.join('tmpOut', 'sent')
if not os.path.exists(sentOutTmp):
	os.makedirs(sentOutTmp)

os.makedirs(debugFolder)

print('\n==============================\nPre-processing input file(s)...\n==============================')
#File splitting: the converter cannot process files that are too big, so they need to be split. For regular-sized sentences, 10,000 sentences per file should do.
#Parameters:
#[1] path to input folder
#[2] encoding of input files
#[3] number of structures per file
#[4] split once ('first'), or every time the threshold in [3] is reached ('all')
#[5] path to temp folder used to store split files
#subprocess.Popen(['python', 'splitFiles.py', inputFolder, 'utf-8', strPerFile, 'all', 'tmpIn'], stderr=subprocess.PIPE).communicate()
# This part creates a tmpIn folder in which all split files are stored
print('\nChecking files to split...\n')
subprocess.call(['python', 'splitFiles.py', inputFolder, 'utf-8', strPerFile, 'all', 'tmpIn'])

#Conversion to format that can be loaded by the .jar.
#Parameters:
#[1] extension of input files (inputFormat)
#[2] path to temp folder used to store split files
# This part creates a folder within the tmpIn folder, which contains files in the CoNLL'09 format with all the information needed for the conversion.
print('\nConverting file format...\n')
if dt == '2':
	originalID = 'no'
convertFolder = 'tmpIn'
subprocess.call(['python', 'conllu2conll.py', inputFormat, convertFolder, originalID, originalForm, originalXpos, parentheses, quotationMarks, orderPunc, orderConj, orderMWE, track, dt, sentOutTmp])

#Scrambling of the files to remove order information.
#Parameters:
#[1]
#[2]

print('\n==============================\nScrambling input file(s)...\n==============================')
# Create output folder if does not exist
# This part takes the enriched CoNLL'09 files and scrambles each file (so that the original order is not explicit anymore). The resulting structures are kept in a folder within tmpIn, with the -scrambled extension.

files2Scramble = [f for f in os.listdir(os.path.join('tmpIn', 'conllu2conll')) if '.conll' in f]
	
for file2Scramble in files2Scramble:
	if scramble == 'yes':
		subprocess.call(['python', 'conllScramble.py', file2Scramble, surfOutTmp, track, dt])
	else:
		print('\nNo file scrambled!')
		copyfile(os.path.join('tmpIn', 'conllu2conll', file2Scramble), os.path.join(surfOutTmp, file2Scramble))

if track == 't2':
	print('\n==============================\nStarting with conversion...\n==============================')
	#Conversion of UD files into Deep representation. A log file is created in the debug folder (log.txt), in which the word 'Error' is printed in case a structure could not be processed. For processing big files, 1g of memory may be needed.
	# This part performs the conversion to deep structures of the scrambled enriched CoNLL'09 structures stored in tmpIn. The results are saved in the tmpOut folder, each file in a folder named as the input file.
	#Parameters:
	#[1] path to input folder
	#[2] -o path to temporary output folder
	list_prefixes = ['en', 'es', 'fr']
	files = [f for f in os.listdir(os.path.join(surfOutTmp)) if '.conll' in f]
	for f in files:
		prefix = f.split('_', 1)[0]
		if prefix not in list_prefixes:
			with open(os.path.join(debugFolder, 'log_deep_processing.txt'), 'a') as logfile:
				proc = subprocess.Popen(['java', '-Xmx1g', '-jar', 'buddy-core-0.1.1-en.jar', os.path.join(surfOutTmp,f), '-o', deepOutTmp], stdout = subprocess.PIPE, universal_newlines=True)
				for line in proc.stdout:
					sys.stdout.write(line)
					logfile.write(line)
		else:
			with open(os.path.join(debugFolder, 'log_deep_processing.txt'), 'a') as logfile:
				proc = subprocess.Popen(['java', '-Xmx1g', '-jar', 'buddy-core-0.1.1-'+prefix+'.jar', os.path.join(surfOutTmp,f), '-o', deepOutTmp], stdout = subprocess.PIPE, universal_newlines=True)
				for line in proc.stdout:
					sys.stdout.write(line)
					logfile.write(line)
			
try:
	shutil.rmtree('tmpIn')
except Exception as e:
	print(e)

print('\n==============================\nConcatenating output files...\n==============================')
#File concatenation: the big files that had been split in smaller files are brought back together.
#Parameters:
#[1] path to input folder
#[2] path to output folder
#[3] encoding of input files
#[4] encoding of output files
#[5] extension of output files (in this case the same as the input format, 'conllu')
#[6] the type of structure that have to be brought together (deep, surf(ace), sent(ences))
if os.path.exists(deepOutTmp):
	dir_contents_deep = [x for x in os.listdir(deepOutTmp) if not x.startswith('.')]
	if len(dir_contents_deep) > 0:
		print('\nConcatenating deep structures...')
		subprocess.call(['python', 'concatenateFiles.py', deepOutTmp, deepOut, 'utf-8', 'utf-8', inputFormat, 'deep', track, dt])

if os.path.exists(surfOutTmp):
	dir_contents_surf = [x for x in os.listdir(surfOutTmp) if not x.startswith('.')]
	if len(dir_contents_surf) > 0:
		print('\nConcatenating surface structures...')
		subprocess.call(['python', 'concatenateFiles.py', surfOutTmp, surfOut, 'utf-8', 'utf-8', inputFormat, 'surf', track, dt])
	
if os.path.exists(sentOutTmp):
	dir_contents_sent = [x for x in os.listdir(sentOutTmp) if not x.startswith('.')]
	if len(dir_contents_sent) > 0:
		print('\nConcatenating sentences...')
		subprocess.call(['python', 'concatenateFiles.py', sentOutTmp, sentOut, 'utf-8', 'utf-8', 'txt', 'sent', track, dt])

try:
	shutil.rmtree('tmpOut')
except Exception as e:
	print(e)

if debug == 'yes':
	print('\n==============================\nChecking outputs...\n==============================')

	try:
		os.remove(os.path.join(debugFolder, 'log_alignments.txt'))
	except Exception as e:
		pass
		
	print('\nChecking alignments between original UD and surface files......\n')
	subprocess.call(['python', 'checkAlignments.py', inputFolder, surfOut, debugFolder, 'utf-8', 'UD2surf', dt, scramble])

	if track == 't2':

		print('\nChecking alignments between surface and deep files......\n')
		subprocess.call(['python', 'checkAlignments.py', surfOut, deepOut, debugFolder, 'utf-8', 'surf2deep', dt, scramble])

		print('\nChecking alignments between original UD and deep files......\n')
		subprocess.call(['python', 'checkAlignments.py', inputFolder, deepOut, debugFolder, 'utf-8', 'UD2deep', dt, scramble])
		
		print('\nChecking deep tree well-formedness...\n')
		#File check: a small script that checks the contents of the output files. It looks for configurations that in theory should not happen: disconnections, cycles, repeated argument numbers, multiple incoming dependencies (in case of tree input). A log file is created in the debug folder (log_treeness.txt), and optionally, folders with the ill-formed files.
		#Parameters:
		#[1] path to output debug folder
		#[2] path to file to be checked
		#[3] encoding of input files
		#[4] type of structure to be checked ('tree' or 'graph')
		#[5] OPTIONAL: path to original files from which the whole conversion started ('inputFolder'). If used, a folder with files that are ill-formed will be created.
		#[6] ONLY IF [5]: input format of original files (inputFormat)
		listFinalFilepaths = glob.glob(os.path.join(deepOut, '*.'+inputFormat))
		for outFile in listFinalFilepaths:
			print(outFile)
			subprocess.call(['python', 'checkWellFormedness.py', debugFolder, outFile, 'utf-8', 'tree', inputFolder, inputFormat])
	else:
		pass
	
stop = timeit.default_timer()
timeConversion = str(datetime.timedelta(seconds=round((stop - start), 2)))
print('\n--------------------\nDONE')
print(timeConversion)
print('--------------------\n')

foTime = codecs.open(os.path.join(debugFolder, 'log_time.txt'),'w','utf-8')
foTime.write(timeConversion)
foTime.close()