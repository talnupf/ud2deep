Download
--------
- Prerequisite: Python 3.5 and Java
- git clone https://gitlab.com/talnupf/ud2deep.git


Quickstart
----------

- Change settings in https://gitlab.com/talnupf/ud2deep/-/blob/master/EN_UD2Deep_V1.0.py (general and conversion parameters, lines 18-52); choose at least: (i) General parameters: input and output folder; (ii) Conversion parameters: track (1 or 2, for creating surface or deep inputs), and data type (1 or 2, for train or test ).

- Paste one or more CoNLL-U input files in the "inputs" folder (note: add prefix en_, es_ or fr_ to your input files to get the language-specific deep conversion; otherwise, by default, the English converter is selected). 

- Run EN_UD2Deep_V1.0.py.

Get T1 structures
-----------------
- Run conversion with track = 't1' parameter.

Get T2 structures
-----------------
- Run conversion with track = 't2' parameter.
- Note: this conversion produces by default t1 structures with no relative order for punctuation signs, so the relative order features in t1 and t2 structures are the same; these slightly different t1 structures may not be useful for the task.

More
----
- To check alignments between T1 and T2 files: use the debug = 'yes' parameter. You can also run the standalone test_align.py file with track = 't2' parameter on t1 and t2 structures produced from the same inputs.
- To get structures in test mode: use dt = '2' parameter, run create_output_templates.py after updating the datasets in the code.
- Some reference input and output structures are provided.


To refer to the SR dataset or the tool, please use the following citation
-----------------
@inproceedings{mille2019second,
  title={The Second Multilingual Surface Realisation Shared Task (SR’19): Overview and Evaluation Results},
  author={Mille, Simon and Belz, Anja and Bohnet, Bernd and Graham, Yvette and Wanner, Leo},
  booktitle={Proceedings of the 2nd Workshop on Multilingual Surface Realisation (MSR 2019)},
  address={Hong Kong, China},
  pages={1--17},
  year={2019}
}

To refer to the deep representations, please use the following citation:
-----------------
@inproceedings{mille-EtAl:2018:INLG,
  title={{U}nderspecified {U}niversal {D}ependency {S}tructures as {I}nputs for {M}ultilingual {S}urface {R}ealisation},
  author={Mille, Simon and Belz, Anja and Bohnet, Bernd and Wanner, Leo},
  booktitle={Proceedings of the 11th International Conference on Natural Language Generation},
  address={Tilburg, The Netherlands},
  pages={199--209},
  year={2018},
}